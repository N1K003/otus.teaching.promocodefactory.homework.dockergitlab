﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                "Customers",
                table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    FirstName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true)
                },
                constraints: table => { table.PrimaryKey("PK_Customers", x => x.Id); });

            migrationBuilder.CreateTable(
                "Preferences",
                table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table => { table.PrimaryKey("PK_Preferences", x => x.Id); });

            migrationBuilder.CreateTable(
                "Roles",
                table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table => { table.PrimaryKey("PK_Roles", x => x.Id); });

            migrationBuilder.CreateTable(
                "CustomerPreference",
                table => new
                {
                    CustomerId = table.Column<Guid>(nullable: false),
                    PreferenceId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CustomerPreference", x => new {x.CustomerId, x.PreferenceId});
                    table.ForeignKey(
                        "FK_CustomerPreference_Customers_CustomerId",
                        x => x.CustomerId,
                        "Customers",
                        "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        "FK_CustomerPreference_Preferences_PreferenceId",
                        x => x.PreferenceId,
                        "Preferences",
                        "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                "Employees",
                table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    FirstName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    RoleId = table.Column<Guid>(nullable: false),
                    AppliedPromocodesCount = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Employees", x => x.Id);
                    table.ForeignKey(
                        "FK_Employees_Roles_RoleId",
                        x => x.RoleId,
                        "Roles",
                        "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                "PromoCodes",
                table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Code = table.Column<string>(nullable: true),
                    ServiceInfo = table.Column<string>(nullable: true),
                    BeginDate = table.Column<DateTime>(nullable: false),
                    EndDate = table.Column<DateTime>(nullable: false),
                    PartnerName = table.Column<string>(nullable: true),
                    PartnerManagerId = table.Column<Guid>(nullable: true),
                    PreferenceId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PromoCodes", x => x.Id);
                    table.ForeignKey(
                        "FK_PromoCodes_Employees_PartnerManagerId",
                        x => x.PartnerManagerId,
                        "Employees",
                        "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        "FK_PromoCodes_Preferences_PreferenceId",
                        x => x.PreferenceId,
                        "Preferences",
                        "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                "IX_CustomerPreference_PreferenceId",
                "CustomerPreference",
                "PreferenceId");

            migrationBuilder.CreateIndex(
                "IX_Employees_RoleId",
                "Employees",
                "RoleId");

            migrationBuilder.CreateIndex(
                "IX_PromoCodes_PartnerManagerId",
                "PromoCodes",
                "PartnerManagerId");

            migrationBuilder.CreateIndex(
                "IX_PromoCodes_PreferenceId",
                "PromoCodes",
                "PreferenceId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                "CustomerPreference");

            migrationBuilder.DropTable(
                "PromoCodes");

            migrationBuilder.DropTable(
                "Customers");

            migrationBuilder.DropTable(
                "Employees");

            migrationBuilder.DropTable(
                "Preferences");

            migrationBuilder.DropTable(
                "Roles");
        }
    }
}